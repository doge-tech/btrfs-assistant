btrfs-assistant (2.1.1-2) UNRELEASED; urgency=medium

  * Bump Standards-Version to 4.7.0, no changes needed

 -- Dale Richards <dale@dalerichards.net>  Thu, 06 Jun 2024 14:56:21 +0100

btrfs-assistant (2.1.1-1) unstable; urgency=medium

  * New upstream version 2.1.1
    - Fix btrfs-assistant script regression impacting wlroots
    - Improve Wayland support
    - Add support for CLI to run in TTY
    - Add minimum free size to the information section
    - When creating snapshots, use errors from btrfs library
    - Port to Qt6
    - Add percentage value display for filesystem utilization
    - Restore CLI snapshots via an index
    - Change timestamp on subvol backups to UTC & ISO format
    - Improve CSV parsing
    - Add ability to modify Snapper snapshot descriptions
    - Add support for QT_STYLE_OVERRIDE
  * d/watch: New GitLab format.
  * d/control:
    - Update build-depends for Qt6.
    - Add dependency on pkexec for privilege elevation.
  * Update man page for new upstream release.
  * Update patches for new upstream release and remove
    patch fixed upstream.

 -- Dale Richards <dale@dalerichards.net>  Wed, 05 Jun 2024 11:52:45 +0100

btrfs-assistant (1.8-1) unstable; urgency=medium

  * Initial release. (Closes: #1059572)

 -- Dale Richards <dale@dalerichards.net>  Fri, 29 Dec 2023 08:02:48 +0000
