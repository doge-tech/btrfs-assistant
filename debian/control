Source: btrfs-assistant
Section: admin
Priority: optional
Maintainer: Dale Richards <dale@dalerichards.net>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 cmake,
 qt6-base-dev,
 qt6-tools-dev,
 libbtrfsutil-dev,
Standards-Version: 4.7.0
Homepage: https://gitlab.com/btrfs-assistant/btrfs-assistant
Vcs-Browser: https://salsa.debian.org/doge-tech/btrfs-assistant
Vcs-Git: https://salsa.debian.org/doge-tech/btrfs-assistant.git

Package: btrfs-assistant
Architecture: any
Multi-Arch: foreign
Depends:
 pkexec,
 ${shlibs:Depends},
 ${misc:Depends},
Recommends:
 snapper,
 btrfsmaintenance,
Description: Qt GUI tool to manage Btrfs filesystems
 Btrfs Assistant is a Qt GUI management tool to make managing a Btrfs
 filesystem easier. The primary features it offers are:
 .
  * An easy to read overview of Btrfs metadata
  * A simple view of subvolumes with or without Snapper/Timeshift snapshots
  * Run and monitor scrub and balance operations
  * A pushbutton method for removing subvolumes
  * A management front-end for Snapper with enhanced restore functionality
  * A front-end for Btrfs Maintenance
